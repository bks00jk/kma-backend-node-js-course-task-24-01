// Квадрат числа
exports.squareNumber = (req, res) => {
    const number = parseFloat(req.body);
    if (isNaN(number) || !isFinite(number)) {
        return res.status(400).send('Invalid input. Please provide a valid number.');
    }
    const square = number * number;
    res.setHeader('Content-Type', 'application/json');
    res.status(200).json({ number: number, square: square });
};

// Реверс тексту
exports.reverseText = (req, res) => {
    const inputText = req.body;
    if (!inputText || typeof inputText !== 'string') {
        return res.status(400).send('Invalid input. Please provide a valid text.');
    }

    const reversedText = inputText.split('').reverse().join('');
    res.setHeader('Content-Type', 'text/plain');
    res.status(200).send(reversedText);
};



// Дані про дату
exports.dateInfo = (req, res) => {
    const { year, month, day } = req.params;
    const parsedYear = parseInt(year, 10);
    const parsedMonth = parseInt(month, 10);
    const parsedDay = parseInt(day, 10);

    if (isNaN(parsedYear) || isNaN(parsedMonth) || isNaN(parsedDay)) {
        return res.status(400).json({ error: 'Invalid input. Please provide valid date parameters.' });
    }

    const inputDate = new Date(parsedYear, parsedMonth - 1, parsedDay);
    if (isNaN(inputDate.getTime())) {
        return res.status(400).json({ error: 'Invalid date' });
    }

    const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const weekDay = weekDays[inputDate.getDay()];
    const isLeapYear = (parsedYear % 4 === 0 && parsedYear % 100 !== 0) || (parsedYear % 400 === 0);
    const currentDate = new Date();
    const difference = Math.abs(Math.floor((currentDate - inputDate) / (1000 * 60 * 60 * 24)));
    res.setHeader('Content-Type', 'application/json');
    res.status(200).json({ weekDay: weekDay, isLeapYear: isLeapYear, difference: difference });
};
