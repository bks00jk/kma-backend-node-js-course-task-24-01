const express = require('express');
const router = express.Router();
const controller = require('./controller');

router.post('/square', express.text(), controller.squareNumber);
router.post('/reverse', express.text(), controller.reverseText);
router.get('/date/:year/:month/:day', controller.dateInfo);

module.exports = router;
